export target

ifeq ($(target), win)
	CC         = i686-w64-mingw32-gcc
	CXX        = i686-w64-mingw32-g++
	#CFLAGS    = empty
	CXXFLASG   = -std=c++11 -DBOOST_NO_CXX11_REF_QUALIFIERS -DBOOST_THREAD_PROVIDES_FUTURE
	INCPATH    = -I. -I/usr/i686-w64-mingw32-posixthread/i686-w64-mingw32/local/include -I/usr/i686-w64-mingw32/sys-root/mingw/local/include
	LIBPATH    = -L/usr/i686-w64-mingw32-posixthread/i686-w64-mingw32/local/lib -L/usr/i686-w64-mingw32/sys-root/mingw/local/lib/opencv-2.4.10
	STATICLIBS = -static-libstdc++ -static-libgcc
	#LDFLAGS    = empty
	LIBS       = -lopencv_highgui2410 -lopencv_imgproc2410 -lopencv_core2410 -lboost_system -lboost_thread_win32 -lyaml-cpp -lws2_32 

else ifeq ($(target),linux)
	CC          = gcc
	CXX         = g++
	CFLAGS      = -DSCV_USE_V4L2
	CXXFLASG    = -std=c++11 -DBOOST_THREAD_PROVIDES_FUTURE -DSCV_USE_V4L2
	INCPATH     = -I. -I/usr/local/include
	#STATICLIBS = empty
	#LDFLAGS    = empty
	LIBS        = -lopencv_highgui -lopencv_imgproc -lopencv_core -lboost_system -lboost_thread -lyaml-cpp -pthread

else ifeq ($(target),arm)
	CC          = arm-linux-gnueabi-gcc
	CXX         = arm-linux-gnueabi-g++
	CFLAGS      = -O3 -DSCV_USE_V4L2
	CXXFLASG    = -O3 -std=c++11 -DBOOST_THREAD_PROVIDES_FUTURE -DSCV_USE_V4L2
	INCPATH     = -I. -I/usr/local/include
	LIBPATH     = -L./lib
	#STATICLIBS = empty
	LDFLAGS     = -s
	LIBS        = -lopencv_highgui -lopencv_imgproc -lopencv_core -ljpeg -lv4l2 -lv4l1 -lv4lconvert -lavcodec -lavformat -lavutil -lswscale -lbz2 -lz -lboost_system -lboost_thread -lboost_atomic -lyaml-cpp -pthread

else
all:
	$(info $$target '${target}' is not known, please use one of following: win, linux, arm)
	$(info example: make target=arm)
	exit
endif

#all: boostpipeline

boostpipeline: v4l2.o pixels.o videoio.o safeostream.o boostpipeline.o
	$(CXX) $(LDFLAGS) $(STATICLIBS) v4l2.o pixels.o videoio.o safeostream.o boostpipeline.o -o boostpipeline $(LIBPATH) $(LIBS)
    
v4l2.o: v4l2.c
	$(CC) $(INCPATH) $(CFLAGS) -c v4l2.c -o v4l2.o

pixels.o: pixels.c
	$(CC) $(INCPATH) $(CFLAGS) -c pixels.c -o pixels.o

videoio.o: videoio.cpp
	$(CXX) $(INCPATH) $(CXXFLASG) -c videoio.cpp -o videoio.o

safeostream.o: safeostream.cpp
	$(CXX) $(INCPATH) $(CXXFLASG) -c safeostream.cpp -o safeostream.o

boostpipeline.o: boostpipeline.cpp
	$(CXX) $(INCPATH) $(CXXFLASG) -c boostpipeline.cpp -o boostpipeline.o

clean:
	rm -f *.o boostpipeline.exe boostpipeline