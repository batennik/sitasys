#ifndef SCV_UDP_SERVER_HPP
#define SCV_UDP_SERVER_HPP

#include <boost/asio.hpp>
#include "traceinfo.hpp"

/*
  simple udp server
  @see at http://www.boost.org/doc/libs/1_59_0/doc/html/boost_asio/example/cpp11/echo/async_udp_echo_server.cpp
*/
using boost::asio::ip::udp;

class udpserver
{
  public:
    udpserver(boost::asio::io_service& io_service, short port, std::function<void(void)> t)
     : io(io_service), task(t), socket_(io_service, udp::endpoint(udp::v4(), port))
    {
        traceinfo << "listening port #7777\n";
        do_receive();
    }

    void do_receive()
    {
        socket_.async_receive_from(
            boost::asio::buffer(data_, max_length), sender_endpoint_,
            [this](boost::system::error_code ec, std::size_t bytes_recvd)
            {
                traceinfo << "a message is received\n";

                //std::thread worker(task);
                //worker.detach();

                // no need for a worker-thread in current impl, just call signal fn
                task();

                do_receive();
            }
        );
    }


    void start()
    {
        io.run();
    }

  private:
    boost::asio::io_service& io;
    std::function<void(void)> task;
    udp::socket socket_;
    udp::endpoint sender_endpoint_;
    enum { max_length = 1024 };
    char data_[max_length];
};

#endif /* SCV_UDP_SERVER_HPP */