#include <opencv2/opencv.hpp>

#include "safeostream.hpp"
#include "traceinfo.hpp"
#include "videoio.hpp"
#include "cvpipeline.hpp"
#include "cvconfig.hpp"
#include "iomodule.hpp"
#include "udpserver.hpp"
#include "cvconfig.hpp"

// possible stage #1
struct module1_t : cvmodule<cv_pipeline_object_t*>
{
    module1_t() {register_name("module1");}

    cv_pipeline_object_t* run(cv_pipeline_object_t* obj_p)
    {
        static int cc = 0;
        traceinfo << "module1 call #" << cc++ << ": " << obj_p << "\n";
        return obj_p;
    }
};

// possible stage #2
struct module2_t : cvmodule<cv_pipeline_object_t*>
{
    module2_t() {register_name("module2");}

    cv_pipeline_object_t* run(cv_pipeline_object_t* obj_p)
    {
        static int cc = 0;
        traceinfo << "module2 call #" << cc++ << ": " << obj_p << "\n";
        return obj_p;
    }
};

// possible stage #3
struct module3_t : cvmodule<cv_pipeline_object_t*>
{
    module3_t() {register_name("module3");}

    cv_pipeline_object_t* run(cv_pipeline_object_t* obj_p)
    {
        static int cc = 0;
        traceinfo << "module3 call #" << cc++ << ": " << obj_p << "\n";
        return obj_p;
    }
};

// can be declared as static
module1_t module1;
module2_t module2;
module3_t module3;

int main()
{
    // init configuration
    cvconfig config;

    // init video in/out
    videoio io(config.pixel_format, cv::Size(config.camera_width, config.camera_height),
               config.frames_before, config.frames_after);

    // io modules
    input_module_t im(config, io);
    termination_module_t tm(config, io);

    // function to server incoming event
    std::function<void(void)> handler = std::bind(&input_module_t::signal, &im);

    // start udp server
    boost::asio::io_service io_service;
    udpserver server(io_service, config.port_number, handler);
    std::thread server_thread(std::bind(&udpserver::start, &server));
    server_thread.detach();

    // contruct the pipe
    pipeline<cv_pipeline_object_t*> pipe(im, tm);

    // add modules on it
    for (std::string m : config.modules) pipe.add(m);

    // run the pipeline
    pipe.run();

    return 0;
}