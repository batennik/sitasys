#ifndef SCV_VIDEO_IO_HPP
#define SCV_VIDEO_IO_HPP

#include <boost/circular_buffer.hpp>
#include <opencv2/opencv.hpp>

#include "traceinfo.hpp"
#include "pixels.h"

struct videoio
{
    videoio(const videoio&) = delete;
    void operator=(const videoio&) = delete;

    videoio(const SCV_PIXEL_FORMAT _format,
            const cv::Size         img_size,
            const int              frames_before,
            const int              frames_after)
      : format(_format), frame_size(img_size), frames(frames_before) {}

    cv::Size get_frame_size() {return frame_size;}

    size_t num_of_frames() {return frames.size();}
    
    // gets the last frame
    cv::Mat& last_frame();

    // get the frame at position
    cv::Mat& frame_at(size_t);

    // stores the frame in the cyclic buffer
    void store_frame(cv::Mat& frame);

    // grabs the frames from camera in infinite loop
    void grabber();

    // dumps buffered frames on the disk
    void dump();

  private:
    std::mutex mtx;
    cv::Size frame_size;
    SCV_PIXEL_FORMAT format;
    boost::circular_buffer<cv::Mat> frames;
};

#endif /* SCV_VIDEO_IO_HPP */