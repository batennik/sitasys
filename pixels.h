#ifndef SCV_PIXELS_H
#define SCV_PIXELS_H

#ifdef __cplusplus
extern "C" {
#endif

/** supported pixel formats */
enum SCV_PIXEL_FORMAT
{
    SCV_PIXEL_FORMAT_YUYV = 0, /* http://linuxtv.org/downloads/v4l-dvb-apis/V4L2-PIX-FMT-YUYV.html */
    SCV_PIXEL_FORMAT_OCV_AUTO  /* use ocv for frame capturing and let 'him' to choose format automatically */
};

/** converts YUV422 image to RGB
 *  @param pptr - pointer to buffer with YUV422 image
 *  @param size - size of the buffer above
 *  @return pointer to the buffer with RBG image
 *  @note the function allocates memory and return pointer to this memory
 *        It is up to called to de-allocate that memory
 */
unsigned char* yuv2rgb(unsigned char* pptr, unsigned int size);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* SCV_PIXELS_H */