#ifndef SCV_CV_CONFIG_HPP
#define SCV_CV_CONFIG_HPP

#include "pixels.h"
#include "yaml-cpp/yaml.h"

#define CONFURATION_FILE_NAME "config.yml"

struct cvconfig
{
    SCV_PIXEL_FORMAT pixel_format;
    unsigned int camera_width;
    unsigned int camera_height;
    unsigned int frames_before;
    unsigned int frames_after;
    unsigned int port_number;
    unsigned int pipe_frame_delay; // in ms
    unsigned int frames_at_event;
    std::vector<std::string> modules; //order is important; input and terminating modules are built-in
    //todo: to be added more later on

    cvconfig(const cvconfig&) = delete;
    void operator=(const cvconfig&) = delete;
    ~cvconfig() {}

    cvconfig()
    {
        YAML::Node config = YAML::LoadFile(CONFURATION_FILE_NAME);

        if (config["pixel-format"])
        {
            if      (!config["pixel-format"].as<std::string>().compare("YUYV"))     pixel_format = SCV_PIXEL_FORMAT_YUYV;
            else if (!config["pixel-format"].as<std::string>().compare("OCV_AUTO")) pixel_format = SCV_PIXEL_FORMAT_OCV_AUTO;
            else
            {
                traceinfo << "pixel-format " << config["pixel-format"].as<std::string>() << " unknown, using OCV_AUTO\n";
                pixel_format = SCV_PIXEL_FORMAT_OCV_AUTO;
            }
        }

        if (config["camera-width"    ]) camera_width     = config["camera-width"    ].as<unsigned int>();
        if (config["camera-height"   ]) camera_height    = config["camera-height"   ].as<unsigned int>();
        if (config["frames-before"   ]) frames_before    = config["frames-before"   ].as<unsigned int>();
        if (config["frames-after"    ]) frames_after     = config["frames-after"    ].as<unsigned int>();
        if (config["port-number"     ]) port_number      = config["port-number"     ].as<unsigned int>();
        if (config["pipe-frame-delay"]) pipe_frame_delay = config["pipe-frame-delay"].as<unsigned int>();
        if (config["frames-at-event" ]) frames_at_event  = config["frames-at-event" ].as<unsigned int>();

        if (config["modules"])
        {
            auto ms = config["modules"];
            for (auto m : ms) modules.push_back(m.as<std::string>());
        }

        traceinfo << "read " << CONFURATION_FILE_NAME << ":\n";
        traceinfo << "pixel-format: "     << pixel_format     << "\n";
        traceinfo << "camera-width: "     << camera_width     << "\n";
        traceinfo << "camera-height: "    << camera_height    << "\n";
        traceinfo << "frames-before: "    << frames_before    << "\n";
        traceinfo << "frames-after: "     << frames_after     << "\n";
        traceinfo << "port-number: "      << port_number      << "\n";
        traceinfo << "pipe-frame-delay: " << pipe_frame_delay << "\n";
        traceinfo << "frames-at-event: "  << frames_at_event  << "\n";
        traceinfo << "modules:\n";
        for (auto m : modules) traceinfo << m << "\n";
    }
};

#endif /* SCV_CV_CONFIG_HPP */
