#ifndef SCV_V4L2_H
#define SCV_V4L2_H

#ifdef __cplusplus
extern "C" {
#endif

/** type of the function, that is supposed to handle the frames */
typedef void (*process_image_ft)(const void* handle_p, const void* data_p, const int size);

/**
 * inits capturing
 * @param function_p - image processing function
 * @param handle     - caller reference, passed to every image processing function call
 */
void v4l2_init(process_image_ft function_p, void* handle);

/** de-inits capturing */
void v4l2_uninit();

/** main routine; never returns */
void v4l2_mainloop();

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* SCV_V4L2_H */