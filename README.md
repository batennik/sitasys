## Sitasys CV project

##### Reference target - AT91SAM9X25: http://www.atmel.com/devices/SAM9X25.aspx
##### Community Sandbox: http://www.at91.com/linux4sam/bin/view/Linux4SAM


## Build requirements
##### gcc - well with g++-4.9, not compilable with g++-4.7
##### boost - works well on version 1.59, not compilable on version 1.53
##### opencv - 2.4.x
##### yaml-cpp - 0.5.x

##### Note: to build for arm a set of native libs is also required.
##### Native libs can be found here or taken from the boards directly:
##### https://drive.google.com/open?id=0ByMs-RowwZp5TGtCVXpYb2dhMDg

## Build
##### version for linux: make target=linux
##### version for target (arm build toolchain required): make target=arm
##### version for win (proper toolchain required): make target=win


## Testing
##### python sendtest.py - sends an udp packet to localhost to verify the app

## Architecture overview
##### see txt verion of readme

<  +--------+   frame   +---------------+
<  | camera |  -------  | cyclic buffer |
<  +--------+           +---------------+
<                                |
<              +-----------------+
<              |
<              | take one frame per an interval
<              |              or
<              | take N frame if 'udp server' signals an even
<              |
<      +--------------------------------------------------------------+
<      |       |                                                      |
<      |      \|/                                                     |
<      |  +---------+          +----------+          +-------------+  |
<      |  |         |          |          |          |             |  |
<      |  |  input  |          | pipeline |          | termination |  |
<      |  | module  |-- ... ---|  moduleN |-- ... ---|    module   |  |
<      |  |         |          |          |          |             |  |
<      |  +---------+          +----------+          +-------------+  |
<      |    /|\                                             |         |
<      |     |                                              |         |
<      +--------------------------------------------------------------+
<            |                                              |
<            |  signal to input                             |
<            |  module an event                             |
<            |                                              |
<   +------------+                     +--------------+     |
<   | udp server |                     | check result |<----+
<   +------------+                     |  dump buffer |
<      ^                               +--------------+
<      | a packet comes
<      |
<
<
<
< Threads map:
<    the thread, that reads the camera and stores a frame in cyclic buffer
<    upd server thread
<    input module thread: sends a frame every pre-configure internal of time
<    termination module thread: terminates the pipe, dumps video file when CV analysis indicates
<    all other modules on pipeline are served by their own threads