#ifndef SCV_CV_PIPELINE_HPP
#define SCV_CV_PIPELINE_HPP

// Original boost.pipeline library from https://github.com/erenon/pipeline
// is used here with small modifications.
// The target platfom (arm-v5) doesn't support lock-free instrutions,
// so the <future> and <promise> from std lib are replaced with
// equal <future> and <promise> from boost, which do not rely on
// lock-free memory updates
// Compiling with BOOST_THREAD_PROVIDES_FUTURE flag is mandatory

#include <boost/pipeline.hpp>
namespace ppl = boost::pipeline;

// abstract rudimentary cv module
template <typename T>
struct pcvmodule
{
  protected:
    pcvmodule() {} //prohibit instantination

  private:
    // bank of modules
    static std::map<std::string, pcvmodule<T>*> bank;

  public:
    // registers the module name in the bank
    void register_name(const std::string& name)
    {
        auto p = std::make_pair(name, this);
        bank.emplace(p);
    }

    // returns pointer to module by its name
    static pcvmodule<T>* get_module(const std::string& name)
    {
        typename std::map<std::string, pcvmodule<T>*>::iterator it = bank.find(name);
        if (it == bank.end()) return NULL;
        else return it->second;
    }
};

// static instance of modules bank
// init_priority() says to compiler to initialize the struct prior others
template <typename T> std::map<std::string, pcvmodule<T>*> pcvmodule<T>::bank __attribute__ ((init_priority (101)));

// base class for cv modules
template <typename T>
struct cvmodule : pcvmodule<T>
{
    // main module routine
    virtual T run(const T) = 0;
};

// function, which wrapps run() method in a cv module
// required as it is not possible to use a class-member-function as pipeline stage
template<typename T>
T module_wrapper(cvmodule<T>* m, T o) {return m->run(o);}

// forward declarations for further usage
template <typename T> class cvpmodule;
template<typename T> void pmodule_wrapper(cvpmodule<T>* m, ppl::queue_back<T> qb);

// base class for data-producers modules
template <typename T>
struct cvpmodule : pcvmodule<T>
{
    cvpmodule(unsigned int ms)
      : delay(ms), lck(mtx), wakeup_reason(std::cv_status::timeout), queue(NULL) {}

    // main module routine
    virtual T produce(void) = 0;

    // signals to module's thread to awake
    void signal() {cv.notify_all();}

    // sends an object to the next module
    void send(T o) {queue->push(o);}

    // the reason of the last wakeup
    std::cv_status wakeup_reason;

  private:
    // delay between produce() calls
    unsigned int delay;

    // unique mutex
    std::mutex mtx;
    std::unique_lock<std::mutex> lck;

    // yields the thread until wanted event
    std::condition_variable cv;

    // to not populate getters/setters
    friend void pmodule_wrapper<T>(cvpmodule*, ppl::queue_back<T>);

    // pointer to the queue towards the next module
    ppl::queue_back<T>* queue;
};

// function, which wrapps produce() method in a producer-module
template<typename T>
void pmodule_wrapper(cvpmodule<T>* m, ppl::queue_back<T> qb)
{
    m->queue = &qb;

    while(1) // run forever
    {
        // call produce and push result to output queue
        qb.push(m->produce());

        // yield for timeout, but until signal on cv
        m->wakeup_reason = m->cv.wait_for(m->lck, std::chrono::milliseconds(m->delay));
    }
}

// binds wrapper function with its first parameter - module pointer
template <typename T>
auto bind_module(cvmodule<T>* mod) -> decltype(std::bind(module_wrapper<T>, mod, std::placeholders::_1))
{
    return std::bind(module_wrapper<T>, mod, std::placeholders::_1);
}

// the same as above but for producer-modules
template <typename T>
auto bind_pmodule(cvpmodule<T>* mod) -> decltype(std::bind(pmodule_wrapper<T>, mod, std::placeholders::_1))
{
    return std::bind(pmodule_wrapper<T>, mod, std::placeholders::_1);
}

// the structure, that helps to unroll the expression like:
// auto pipe = ppl::from(iv) | st1 | st2 | ... | stN
// this is a set N of structures, where every N-th struct defines
// a static method join() to calculate the N-th stage
// the methon run() runs the pipeline
template <typename T, unsigned int N> struct pipe_unroller
{
    // recursively calls join() from prev stage and returns value of 'prev_call() | next_module'
    static auto join(std::vector<cvmodule<T>*>& mv, std::vector<T>& iv) -> decltype(pipe_unroller<T,N-1>::join(mv, iv) | bind_module(mv[N-1]))
    {
        return pipe_unroller<T,N-1>::join(mv, iv) | bind_module(mv[N-1]);
    }

    // the same as above, but takes producer-module instead of input vector
    static auto join2(std::vector<cvmodule<T>*>& mv, cvpmodule<T>& im) -> decltype(pipe_unroller<T,N-1>::join2(mv, im) | bind_module(mv[N-1]))
    {
        return pipe_unroller<T,N-1>::join2(mv, im) | bind_module(mv[N-1]);
    }

    // calls join() to construct the pipe and runs it
    static void run(std::vector<cvmodule<T>*>& mv, std::vector<T>& iv, std::vector<T>& ov)
    {
        if (N > mv.size())
        {
            // drop to prev instance of unroller to get the pipe with proper length
            pipe_unroller<T,N-1>::run(mv, iv, ov);
        }
        else
        {
            // construct the pipe
            auto pipe = pipe_unroller<T,N>::join(mv, iv) | ov;
            
            // init pool of threads
            ppl::thread_pool pool{mv.size()};
            
            // execute it
            auto exec = pipe.run(pool);
            exec.wait();
        }
    }

    // the same as above, but takes producer-module instead of input vector
    static void run2(std::vector<cvmodule<T>*>& mv, cvpmodule<T>& im, std::vector<T>& ov)
    {
        if (N > mv.size())
        {
            // drop to prev instance of unroller to get the pipe with proper length
            pipe_unroller<T,N-1>::run2(mv, im, ov);
        }
        else
        {
            // construct the pipe
            auto pipe = pipe_unroller<T,N>::join2(mv, im) | ov;
            
            // init pool of threads
            ppl::thread_pool pool{mv.size() + 1}; //plus one for producer
            
            // execute it
            auto exec = pipe.run(pool);
            exec.wait();
        }
    }
};

// explicit instantiation if the last unroller helper
template <typename T> struct pipe_unroller<T, 0u>
{
    // the first piece of the pipe
    static auto join(std::vector<cvmodule<T>*>& mv, std::vector<T>& iv) -> decltype(ppl::from(iv))
    {
        return ppl::from(iv);
    }

    // the same as above, but takes producer-module instead of input vector
    static auto join2(std::vector<cvmodule<T>*>& mv, cvpmodule<T>& im) -> decltype(ppl::from<T>(bind_pmodule(&im)))
    {
        return ppl::from<T>(bind_pmodule(&im));
    }

    // empty
    static void run(std::vector<cvmodule<T>*>& mv, std::vector<T>& iv, std::vector<T>& ov) {}
    static void run2(std::vector<cvmodule<T>*>& mv, cvpmodule<T>& im, std::vector<T>& ov) {}
};

// the max number of possible stages;
// note: by increasing this values, more unroller instances
// are populated, which could have an impact on binary size
#ifndef CV_PIPELINE_MAX_NUM_OF_STAGES
#define CV_PIPELINE_MAX_NUM_OF_STAGES 5u
#endif

template<typename T>
struct pipeline
{
    pipeline() {};
    pipeline(const pipeline&) = delete;
    void operator=(const pipeline&) = delete;

    pipeline(std::vector<cvmodule<T>*> _mv): mv(_mv), im(NULL), tm(NULL) {}
    pipeline(cvpmodule<T>* _im, std::vector<cvmodule<T>*> _mv, cvmodule<T>* _tm): im(_im), tm(_tm), mv(_mv) {}
    pipeline(cvpmodule<T>& _im, cvmodule<T>& _tm): im(&_im), tm(&_tm) {}

    // to add modules on pipeline by a pointer to its instance
    void add2(cvmodule<T>* m) {mv.push_back(m);}

    // to add modules on pipeline by its name
    void add(const std::string& name)
    {
        // todo: only cvmodule<T>* pointers are allowed to be stored
        cvmodule<T>* m = static_cast<cvmodule<T>*>(pcvmodule<T>::get_module(name));
        if (!m) traceinfo << name << " is not a valid module name\n";
        else add2(m);
    }

    // takes one object and passes it through the pipe
    T run(T in)
    {
        std::vector<T> iv = {in};
        std::vector<T> ov;

        pipe_unroller<T,CV_PIPELINE_MAX_NUM_OF_STAGES>::run(mv, iv, ov);

        return ov[0];
    }

    // if producer and termination modules are pre-configured
    T run()
    {
        if (im && tm)
        {
            std::vector<T> ov;

            mv.push_back(tm);
            pipe_unroller<T,CV_PIPELINE_MAX_NUM_OF_STAGES>::run2(mv, *im, ov);

            // the pipe is supposed to run forever if producer-module is written properly, in case if not:
            mv.pop_back();
            return ov[0];
        }
    }

    // for the modules, which implementes io by ifself
    void run2()
    {
        run(T());
    }

    ~pipeline() {}

  private:
    std::vector<cvmodule<T>*> mv;
    cvpmodule<T>* im; //input module
    cvmodule<T>* tm;  // termination module
};


/* usage example:

struct cv_pipeline_object_t {};

//possible stage#1: takes the last frame from the buffer and passes to the pipeline
struct module1_t : cvmodule<cv_pipeline_object_t*>
{
    module1_t() {register_name("module1_t");}

    cv_pipeline_object_t* run(cv_pipeline_object_t* obj_p)
    {
        static int cc = 0;
        traceinfo << "module1 call #" << cc++ << "\n";
        traceinfo << obj_p << "\n";

        // contruct cv pipeline object with the last frame in the buffer
        // the incoming pointer is ignored
        return new cv_pipeline_object_t();
    }
};

// possible stage #1
struct module2_t : cvmodule<cv_pipeline_object_t*>
{
    module2_t() {register_name("module2_t");}

    cv_pipeline_object_t* run(cv_pipeline_object_t* obj_p)
    {
        static int cc = 0;
        traceinfo << obj_p << "\n";
        traceinfo << "module2 call #" << cc++ << "\n";
        return obj_p;
    }
};

// possible stage #2
struct module3_t : cvmodule<cv_pipeline_object_t*>
{
    module3_t() {register_name("module3_t");}
    
    cv_pipeline_object_t* run(cv_pipeline_object_t* obj_p)
    {
        static int cc = 0;
        traceinfo << "module3 call #" << cc++ << "\n";
        return obj_p;
    }
};

// possible finishing stage: shows the frame
struct module4_t : cvmodule<cv_pipeline_object_t*>
{
    module4_t() {register_name("module4_t");}

    cv_pipeline_object_t* run(cv_pipeline_object_t* obj_p)
    {
        static int cc = 0;
        traceinfo << "module4 call #" << cc++ << "\n";

        cv::imshow("out", obj_p->get_frame());
        cv::waitKey(0);

        // the object many be released here if required
        delete obj_p;

        return NULL;
    }
};

int main()
{
    module1_t module1;
    module2_t module2;
    module3_t module3;
    module4_t module4;

    std::vector<cvmodule<cv_pipeline_object_t*>*> modules = {&module1, &module2, &module3, &module4};
    pipeline<cv_pipeline_object_t*> pipe(modules);
    pipe.run2();
    
    return 0;
}

int main2()
{
    module1_t module1;
    module2_t module2;
    module3_t module3;
    module4_t module4;

    cv_pipeline_object_t* obj_p = new cv_pipeline_object_t();

    pipeline<cv_pipeline_object_t*> pipe;
    pipe.add("module1_t");
    pipe.add("module2_t");
    pipe.add("module3_t");
    pipe.add("module4_t");

    obj_p = pipe.run(obj_p);

    delete obj;
    
    return 0;
}

*/

#endif /* SCV_CV_PIPELINE_HPP */