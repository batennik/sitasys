#include "v4l2.h"
#include "pixels.h"
#include "videoio.hpp"

// callback function passed to v4l2
void v4l2_process_image(const void* handle, const void* data, const int size)
{
    static int cc = 0;
    traceinfo << "grabbing the frame #" << cc++ << "\n";

    videoio* io = (videoio*)handle;

    // cv::Mat is used as a wrapping container for YUYV image
    cv::Mat frame = cv::Mat(io->get_frame_size().height, io->get_frame_size().width, CV_16UC1, const_cast<void*>(data));

    // store a copy; orig buffer is reused
    io->store_frame(frame);
}

void videoio::grabber()
{
    switch (format)
    {
        case SCV_PIXEL_FORMAT_YUYV:
        {
            // init v4l2 and go to its main loop
            // todo: apply pixel format to the camera
            v4l2_init(v4l2_process_image, (void*)this);
            v4l2_mainloop();
            break;
        }

        case SCV_PIXEL_FORMAT_OCV_AUTO:
        {
            cv::Mat frame;
            static int cc = 0;

            // init camera
            cv::VideoCapture cap(0);
        
            if (!cap.isOpened())
            {
                traceinfo << "can't bind camera device!\n";
                return;
            }

            // set camera options
            cap.set(CV_CAP_PROP_FRAME_WIDTH, frame_size.width);
            cap.set(CV_CAP_PROP_FRAME_HEIGHT, frame_size.height);

            // grab frames for ever
            for(;;)
            {
                traceinfo << "grabbing the frame #" << cc++ << "\n";

                cap >> frame;
                store_frame(frame);

                // sleep for some time
                //boost::this_thread::sleep_for(boost::chrono::milliseconds(40));
            }
            break;
        }

        defalut: traceinfo << "error: unknown pixel format " << format << "\n";
    }
}

void videoio::dump()
{
    traceinfo << "dumping " << frames.size() << " frames..." << "\n";

    // todo: calculate fps in run-time, saving with fsp=30
    cv::VideoWriter vw("out.avi", CV_FOURCC('F','M','P','4'), 30, frame_size);

    // lock the mutex to exclude access from buffer writer
    mtx.lock();

    for (unsigned int ii = 0; ii < frames.size(); ++ii)
    {
        switch (format)
        {
            case SCV_PIXEL_FORMAT_YUYV:
            {
                // conversion from YUYV to RGB and saving to disk
                unsigned char* rgb_p = yuv2rgb(frames[ii].data, 2*frame_size.height*frame_size.width);
                cv::Mat frame(frame_size.height, frame_size.width, CV_8UC3, (void*)rgb_p);
                vw << frame;
                free(rgb_p);
                break;
            }

            case SCV_PIXEL_FORMAT_OCV_AUTO:
            {
                vw << frames[ii];
                break;
            }

            defalut: traceinfo << "error: unknown pixel format " << format << "\n";
        }
    }

    // unlock the mtx
    mtx.unlock();

    traceinfo << "written to file out.avi\n";
    vw.release();
}

// gets the last frame
cv::Mat& videoio::last_frame()
{
    static cv::Mat f;
    cv::Mat& frame = f;

    mtx.lock();
    // todo: an empty frame is returned if the buffer is empty
    if (!frames.empty()) frame = frames.back();
    mtx.unlock();
    
    return frame;
}

// get the frame at position
cv::Mat& videoio::frame_at(size_t ix)
{
    static cv::Mat f;
    cv::Mat& frame = f;

    mtx.lock();
    // todo: an empty frame is returned if the buffer is empty
    if (ix < frames.size()) frame = frames.back();
    mtx.unlock();
    
    return frame;
}

// stores the frame in the cyclic buffer
void videoio::store_frame(cv::Mat& frame)
{
    mtx.lock();
    frames.push_back(frame.clone());
    mtx.unlock();
}
