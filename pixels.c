#include <stdlib.h>
#include "pixels.h"

/**
 * This is probably the most acceptable conversion from camera YUYV to RGB
 *
 * Wikipedia has a good discussion on the details of various conversions and cites good references:
 * http:*en.wikipedia.org/wiki/YUV
 *
 * Also http:*www.fourcc.org/yuv.php
 *
 * What's not clear without knowing more about the camera in question is how often U & V are sampled compared
 * to Y.
 *
 * E.g. YUV444, which is equivalent to RGB, where both require 3 bytes for each pixel
 *      YUV422, which we assume here, where there are 2 bytes for each pixel, with two Y samples for one U & V,
 *              or as the name implies, 4Y and 2 UV pairs
 *      YUV420, where for every 4 Ys, there is a single UV pair, 1.5 bytes for each pixel or 36 bytes for 24 pixels
 *
 * See more at http://ecee.colorado.edu/~siewerts/extra/ecen5043/ecen5043_code/va-opencv-examples/capture-viewer/capture.cpp
 */
void _yuv2rgb(int y, int u, int v, unsigned char* r, unsigned char* g, unsigned char* b)
{
   int r1, g1, b1;

   // replaces floating point coefficients
   int c = y-16, d = u - 128, e = v - 128;       

   // Conversion that avoids floating point
   r1 = (298 * c           + 409 * e + 128) >> 8;
   g1 = (298 * c - 100 * d - 208 * e + 128) >> 8;
   b1 = (298 * c + 516 * d           + 128) >> 8;

   // Computed values may need clipping.
   if (r1 > 255) r1 = 255;
   if (g1 > 255) g1 = 255;
   if (b1 > 255) b1 = 255;

   if (r1 < 0) r1 = 0;
   if (g1 < 0) g1 = 0;
   if (b1 < 0) b1 = 0;

   *r = r1 ;
   *g = g1 ;
   *b = b1 ;
}

/** converts YUV422 image to RGB
 *  @param pptr - pointer to buffer with YUV422 image
 *  @param size - size of the buffer above
 *  @return pointer to the buffer with RBG image
 *  @note the function allocates memory and return pointer to this memory
 *        It is up to called to de-allocate that memory
 *
 * See more at http://ecee.colorado.edu/~siewerts/extra/ecen5043/ecen5043_code/va-opencv-examples/capture-viewer/capture.cpp
 */
 unsigned char* yuv2rgb(unsigned char* pptr, unsigned int size)
{
   unsigned int i = 0, newi = 0;
   unsigned int y_temp, y2_temp, u_temp, v_temp;

   unsigned char* bigbuffer = (unsigned char*)malloc(sizeof(char)*((size/2u)*3u));
   
   for (i = 0, newi = 0; i < size; i = i + 4, newi = newi + 6)
   {
      y_temp=(int)pptr[i]; u_temp=(int)pptr[i+1]; y2_temp=(int)pptr[i+2]; v_temp=(int)pptr[i+3];
      _yuv2rgb(y_temp, u_temp, v_temp, &bigbuffer[newi], &bigbuffer[newi+1], &bigbuffer[newi+2]);
      _yuv2rgb(y2_temp, u_temp, v_temp, &bigbuffer[newi+3], &bigbuffer[newi+4], &bigbuffer[newi+5]);
   }

   return bigbuffer;
}