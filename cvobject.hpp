#ifndef SCV_CV_OBJECT_HPP
#define SCV_CV_OBJECT_HPP

// the type of object which is passed through the pipeline
struct cv_pipeline_object_t
{
    cv_pipeline_object_t() : frame(), analysis_result_positive(false) {} //emtpy
    cv_pipeline_object_t(const cv_pipeline_object_t&) = delete;
    void operator=(const cv_pipeline_object_t&) = delete;
    cv_pipeline_object_t(cv::Mat& f) : frame(f), analysis_result_positive(false) { } // new Mat constructed, no data copied
    ~cv_pipeline_object_t() {}

    cv::Mat frame;
    bool analysis_result_positive;
    // todo: to be expanded later on with meta data
};

#endif /* SCV_CV_OBJECT_HPP */