#ifndef SCV_TRACE_INFO_HPP
#define SCV_TRACE_INFO_HPP

#include <boost/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "safeostream.hpp"

/* possible usage:
   traceinfo << "output text"; */
#define traceinfo safe_cout << boost::posix_time::microsec_clock::local_time() << \
                               ", pid=" << getpid() << \
                               ", tid=" << /* in hex */ boost::this_thread::get_id() << ": "

#endif /* SCV_TRACE_INFO_HPP */