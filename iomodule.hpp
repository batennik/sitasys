#ifndef SCV_IO_MODULE_HPP
#define SCV_IO_MODULE_HPP

#include "traceinfo.hpp"
#include "videoio.hpp"
#include "cvobject.hpp"
#include "cvpipeline.hpp"
#include "cvconfig.hpp"

// input module
struct input_module_t : cvpmodule<cv_pipeline_object_t*>
{
    input_module_t(cvconfig& cnf, videoio& _io)
      : cvpmodule(cnf.pipe_frame_delay), config(cnf), io(_io)
    {
        register_name("input_module");

        // start grabber thread and detach it
        std::thread grabber(std::bind(&videoio::grabber, &io));
        grabber.detach();
    }

    cv_pipeline_object_t* produce()
    {
        switch (wakeup_reason)
        {
            case std::cv_status::timeout:
            {
                static int cc = 0;

                cv_pipeline_object_t* obj_p = new cv_pipeline_object_t(io.last_frame());
                traceinfo << "input_module call #" << cc++ << ": " << obj_p << "\n";

                return obj_p;
            }
            case std::cv_status::no_timeout:
            {
                traceinfo << "input_module: sending " << config.frames_at_event << " frames to the pipe\n";

                // todo: lock the io buffer, as the grabber thread adds frames into the buffer concurrently
                for (int ii = config.frames_at_event - 1; ii > 0; --ii)
                {
                    cv_pipeline_object_t* obj_p = new cv_pipeline_object_t(io.frame_at(io.num_of_frames() - ii));
                    send(obj_p);

                    // yield this thread to allow next stage start processing
                    std::this_thread::yield();
                }

                cv_pipeline_object_t* obj_p = new cv_pipeline_object_t(io.last_frame());
                obj_p->analysis_result_positive = true; // todo: to be removed
                return obj_p;
            }
            default: break;
        }
    }

    cvconfig& config;
    videoio& io;
};

// termination module
struct termination_module_t : cvmodule<cv_pipeline_object_t*>
{
    termination_module_t(cvconfig& cnf, videoio& _io)
      : config(cnf), io(_io) {register_name("termination_module");}

    cv_pipeline_object_t* run(cv_pipeline_object_t* obj_p)
    {
        static int cc = 0;
        traceinfo << "termination_module call #" << cc++ << ": " << obj_p << "\n";

        // check result of analysis and do necessary actions
        if (obj_p)
        {
            if (obj_p->analysis_result_positive)
            {
                io.dump();
            }
        }

        delete obj_p;
        return NULL;
    }

    cvconfig& config;
    videoio& io;
};

#endif /* SCV_IO_MODULE_HPP */